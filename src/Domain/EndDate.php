<?php

namespace Cdv\Domain;

class EndDate
{
    /**
     * @var \DateTime
     */
    private $endDate;

    public function __construct(\DateTime $endDate)
    {
        $this->validateEndDate($endDate);

        $this->endDate = $endDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return EndDate
     */
    public function setEndDate(\DateTime $endDate): EndDate
    {
        $this->validateEndDate($endDate);

        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        $now = new \DateTime('now');

        return $this->endDate < $now;
    }

    /**
     * @param \DateTime $endDate
     * @return bool
     */
    private function validateEndDate(\DateTime $endDate): bool
    {
        $now = new \DateTime('now');

        if ($endDate < $now) {
            throw new \LogicException('EndDate must be in future');
        }

        return true;
    }
}