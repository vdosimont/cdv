<?php

namespace Cdv\Domain\DiscountCode;

class DiscountType
{
    const PERCENT_TYPE = 'percent';
    const FIXED_TYPE = 'fixed';

    private $type;

    /**
     * DiscountType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->validateType($type);

        $this->type = $type;
    }

    /**
     * @return string|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string|string $type
     * @return DiscountType
     */
    public function setType(string $type): DiscountType
    {
        $this->validateType($type);

        $this->type = $type;
        return $this;
    }

    /**
     * @param string $type
     * @return bool
     */
    private function validateType(string $type): bool
    {
        $authorizedTypes = [
            self::FIXED_TYPE,
            self::PERCENT_TYPE
        ];

        if  (!\in_array($type, $authorizedTypes, true)){
            throw new \LogicException('Can\'t apply DiscountType', 500);
        }

        return true;
    }
}