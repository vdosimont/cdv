<?php

namespace Cdv\Domain\DiscountCode;

class DiscountDefinition
{
    private $definition;

    public function __construct(string $definition)
    {
        $this->validateDefinition($definition);

        $this->definition = $definition;
    }

    /**
     * @return string
     */
    public function getDefinition(): string
    {
        return $this->definition;
    }

    /**
     * @param string $definition
     * @return DiscountDefinition
     */
    public function setDefinition(string $definition): DiscountDefinition
    {
        $this->validateDefinition($definition);

        $this->definition = $definition;
        return $this;
    }

    /**
     * @param string $definition
     * @return bool
     */
    private function validateDefinition(string $definition): bool
    {
        if (!\is_string($definition) && empty($definition)){
            throw new \LogicException('Can\'t define non-string or empty DiscountDefinition', 500);
        }

        return true;
    }
}