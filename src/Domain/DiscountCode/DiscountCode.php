<?php

namespace Cdv\Domain\DiscountCode;

use Cdv\Domain\EndDate;

class DiscountCode
{
    /**
     * Not use here (it's the discountCode name)
     * We may use it to retrieve it the DB
     * @var DiscountDefinition
     */
    private $discountDefinition;

    /**
     * Percent or Fixed
     * @var DiscountType
     */
    private $discountType;

    /**
     * @var EndDate
     */
    private $endDate;

    /**
     * @var DiscountMaxUse
     */
    private $maxUse;

    /**
     * @var bool
     */
    private $revoked;

    /**
     * @var DiscountAmount
     */
    private $amount;

    /**
     * @var int
     */
    private $useCount;

    public function __construct(
        DiscountDefinition $discountDefinition, DiscountType $discountType, EndDate $endDate,
        DiscountMaxUse $maxUse, DiscountAmount $amount
    )
    {
        $this->discountDefinition = $discountDefinition;
        $this->discountType = $discountType;
        $this->endDate = $endDate;
        $this->maxUse = $maxUse;
        $this->revoked = false;
        $this->amount = $amount;
        $this->useCount = 0;
    }

    /**
     * Applies DiscountCode, check if it can be applied, decrement useCount
     * @return float
     */
    public function apply(): float
    {
        if ($this->revoked || $this->endDate->isExpired() || !$this->checkUseCount()) {
            throw new \LogicException('Can\'t apply DiscountCode', 500);
        }

        $this->useOnce();
        return $this->amount->getAmount();
    }

    /**
     * Revoke DiscountCode, can't be used anymore.
     */
    public function revoke(): void
    {
        $this->revoked = true;
    }

    /**
     * Check if useCount is smaller than maxUse
     * @return bool
     */
    private function checkUseCount(): bool
    {
        return $this->maxUse->canUse($this->useCount);
    }

    /**
     * @return bool
     */
    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->discountType->getType();
    }

    /**
     * Increment useCount when DiscountCode is used
     */
    private function useOnce(): void
    {
        $this->useCount++;
    }

    /**
     * @return int
     */
    public function getUseCount(): int
    {
        return $this->useCount;
    }
}