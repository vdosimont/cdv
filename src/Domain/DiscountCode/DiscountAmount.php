<?php

namespace Cdv\Domain\DiscountCode;

class DiscountAmount
{
    /**
     * @var float
     */
    private $amount;

    public function __construct(float $amount)
    {
        $this->validateAmount($amount);

        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return DiscountAmount
     */
    public function setAmount(float $amount): DiscountAmount
    {
        $this->validateAmount($amount);

        $this->amount = $amount;
        return $this;
    }

    private function validateAmount(float $amount)
    {
        if ($amount < 0) {
            throw new \LogicException('Amount needs to be greater than or equal 0', 500);
        }
    }
}