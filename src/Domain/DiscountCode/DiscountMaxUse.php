<?php

namespace Cdv\Domain\DiscountCode;

class DiscountMaxUse
{
    /**
     * @var int
     */
    private $maxUse;

    public function __construct(int $maxUse = 0)
    {
        $this->validateMaxUse($maxUse);

        $this->maxUse = $maxUse;
    }

    /**
     * @param int $useCount
     * @return bool
     */
    public function canUse(int $useCount): bool
    {
        return $this->maxUse > 0 && $useCount < $this->maxUse;
    }

    /**
     * @return int
     */
    public function getMaxUse(): int
    {
        return $this->maxUse;
    }

    /**
     * @param int $maxUse
     * @return DiscountMaxUse
     */
    public function setMaxUse(int $maxUse): DiscountMaxUse
    {
        $this->validateMaxUse($maxUse);

        $this->maxUse = $maxUse;
        return $this;
    }

    private function validateMaxUse(int $maxUse)
    {
        if ($maxUse < 0) {
            throw new \LogicException('MaxUse needs to be greater than or equal 0', 500);
        }
    }
}