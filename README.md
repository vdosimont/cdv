Run php builtin webserver:

```
$ php -S localhost:8000 -d display_errors=1 -t public/
```

Run tests
```
$ vendor/bin/phpunit tests
```

/public -> here to be here (public files i.e index.php)

/src -> contains source code

/tests -> source code tests