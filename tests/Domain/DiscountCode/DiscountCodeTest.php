<?php

namespace Tests\Domain\DiscountCode;

use Cdv\Domain\DiscountCode\DiscountAmount;
use Cdv\Domain\DiscountCode\DiscountCode;
use Cdv\Domain\DiscountCode\DiscountDefinition;
use Cdv\Domain\DiscountCode\DiscountMaxUse;
use Cdv\Domain\DiscountCode\DiscountType;
use Cdv\Domain\EndDate;
use PHPUnit\Framework\TestCase;

class DiscountCodeTest extends TestCase
{
    /**
     * @var DiscountCode
     */
    public $discountCode;

    public function setUp()
    {
        $this->discountCode =  new DiscountCode(
            new DiscountDefinition('CODE001'),
            new DiscountType(DiscountType::FIXED_TYPE),
            new EndDate(new \DateTime('+ 15 days')),
            new DiscountMaxUse(50),
            new DiscountAmount(15)
        );
    }

    public function testDiscountCodeInstantiationSucceed()
    {
        $this->assertSame(DiscountType::FIXED_TYPE, $this->discountCode->getType());
        $this->assertFalse(false, $this->discountCode->isRevoked());
    }

    public function testDiscountCodeCanBeRevoked()
    {
        $this->assertFalse(false, $this->discountCode->isRevoked());
        $this->discountCode->revoke();

        $this->assertTrue($this->discountCode->isRevoked());
    }

    public function testDiscountCodeCantBeUsedWhenExpired()
    {
        $endDateMock = $this->getMockBuilder(EndDate::class)->disableOriginalConstructor()->getMock();

        $endDateMock
            ->expects($this->once())
            ->method('isExpired')
            ->willReturn(true)
        ;

        $discountCode = new DiscountCode(
            new DiscountDefinition('CODE001'),
            new DiscountType(DiscountType::FIXED_TYPE),
            $endDateMock,
            new DiscountMaxUse(50),
            new DiscountAmount(15)
        );

        $this->expectException(\LogicException::class);
        $discountCode->apply();
    }

    /**
     * @dataProvider parametersProvider
     */
    public function testApplyReturnsDiscountCodeAmout($definition, $type, $endDate, $maxUse, $amount)
    {
        $discountCode = new DiscountCode(
            new DiscountDefinition($definition),
            new DiscountType($type),
            new EndDate($endDate),
            new DiscountMaxUse($maxUse),
            new DiscountAmount($amount)
        );

        $this->assertEquals($amount, $discountCode->apply());
        $this->assertEquals(1, $discountCode->getUseCount());
    }


    public function parametersProvider()
    {
        return [
            ['CODE001',  DiscountType::FIXED_TYPE, new \DateTime('+ 15 days'), 50, 15],
            ['CODE002',  DiscountType::FIXED_TYPE, new \DateTime('+ 15 days'), 50, 25],
            ['CODE003',  DiscountType::PERCENT_TYPE, new \DateTime('+ 15 days'), 50, 35],
            ['CODE004',  DiscountType::FIXED_TYPE, new \DateTime('+ 15 days'), 50, 45.56],
            ['CODE005',  DiscountType::PERCENT_TYPE, new \DateTime('+ 15 days'), 50, 77],
            ['CODE006',  DiscountType::FIXED_TYPE, new \DateTime('+ 15 days'), 50, 100],
            ['CODE007',  DiscountType::PERCENT_TYPE, new \DateTime('+ 15 days'), 50, 50],
        ];
    }
}